import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

import mods.primaltech.ClayKiln;
import mods.primaltech.WoodenBasin;

mods.compatskills.Requirement.addRequirement(<betterwithmods:cooking_pot:1>, "reskillable:building|3", "reskillable:magic|2", "reskillable:farming|2");
recipes.remove(<betterwithmods:cooking_pot:1>);
recipes.addShaped(<betterwithmods:cooking_pot:1>,
 [[<ore:plateCopper>,<ore:bone>, <ore:plateCopper>],
  [<ore:plateCopper>,<minecraft:water_bucket>, <ore:plateCopper>],
  [<ore:plateCopper>,<ore:plateCopper>, <ore:plateCopper>]]);


furnace.remove(<ore:ingotBrick>);

recipes.remove(<minecraft:crafting_table>);
recipes.addShaped(<minecraft:crafting_table>,
 [[<tconstruct:binding>.withTag({Material: "copper"}),<minecraft:shears>, <tconstruct:binding>.withTag({Material: "copper"})],
  [<tconstruct:hatchet>,<primal_tech:work_stump>, <ore:ticSaw>],
  [<tconstruct:binding>.withTag({Material: "copper"}),<ore:slabWood>, <tconstruct:binding>.withTag({Material: "copper"})]]);
 
ClayKiln.addRecipe(<tconstruct:materials>, <tconstruct:soil>, 200);


recipes.remove(<minecraft:compass>);
recipes.addShaped(<minecraft:compass>,
 [[null,<ore:plateIron>, <ore:dustRedstone>],
  [<ore:plateIron>,<tconstruct:arrow_head>.withTag({Material: "iron"}), <ore:plateIron>],
  [<ore:dyeBlue>,<ore:plateIron>, null]]);

recipes.remove(<betterwithmods:single_machine>);
recipes.addShaped(<betterwithmods:single_machine>,
 [[<ore:plateIron>,<betterwithmods:material>, <ore:plateIron>],
  [<ore:plateIron>,<betterwithmods:wooden_axle>, <ore:plateIron>],
  [<minecraft:stone_slab>,<minecraft:stone_slab>, <minecraft:stone_slab>]]);
  
recipes.remove(<tconstruct:tooltables:3>);
recipes.addShaped(<tconstruct:tooltables:3>,
 [[<tinkersurvival:crude_knife:*>,null],
  [<primal_tech:work_stump>, null]]);
  

furnace.remove(<minecraft:netherbrick>);
furnace.remove(<astralsorcery:itemcraftingcomponent:1>); 
mods.astralsorcery.LightTransmutation.removeTransmutation(<astralsorcery:blockcustomore:1>, false);

mods.astralsorcery.Altar.addAttunmentAltarRecipe("mypackname:shaped/internal/altar/iguessmarble", <astralsorcery:itemcraftingcomponent:1>, 500, 300, [
            <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "ordo"}]}), <forge:bucketfilled>.withTag({FluidName: "astralsorcery.liquidstarlight", Amount: 1000}), <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "ordo"}]}),
            <thaumcraft:salis_mundus>, <ore:oreIron>, <thaumcraft:salis_mundus>,
            <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "ordo"}]}), <thaumcraft:salis_mundus>, <thaumcraft:crystal_essence>.withTag({Aspects: [{amount: 1, key: "ordo"}]}),
            <ore:gemAquamarine>, <ore:gemAquamarine>, <ore:gemAquamarine>, <ore:gemAquamarine>]);
			


  
mods.betterwithmods.Cauldron.addUnstoked([<ore:slimeball>], [<betterwithmods:material:12>]);
mods.betterwithmods.Cauldron.addUnstoked([<minecraft:rotten_flesh> * 2], [<betterwithmods:material:12>]);
mods.betterwithmods.Cauldron.remove([<betterwithmods:material:35>]);
mods.betterwithmods.Cauldron.addStoked([<ore:dustPotash>, <betterwithmods:material:16> * 4, <forge:bucketfilled>.withTag({FluidName: "creosote", Amount: 1000})], [<betterwithmods:material:35> * 8, <minecraft:bucket>]);


recipes.remove(<betterwithmods:axle_generator>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("WindMill", "", 20, [<aspect:aer>, <aspect:ordo>], <betterwithmods:axle_generator>, 
					[[null, <betterwithmods:material:11>, null], 
					 [<betterwithmods:material:11>, <ore:gearWood>, <betterwithmods:material:11>,], 
					 [null, <betterwithmods:material:11>, null]]);
					 
recipes.remove(<betterwithmods:axle_generator:2>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("WindMillB", "", 40, [<aspect:aer>, <aspect:ordo>], <betterwithmods:axle_generator:2>, 
					[[<betterwithmods:material:11>, <betterwithmods:material:11>, <betterwithmods:material:11>], 
					 [<betterwithmods:material:11>, <ore:gearWood>, <betterwithmods:material:11>,], 
					 [<betterwithmods:material:11>, <betterwithmods:material:11>, <betterwithmods:material:11>]]);
					 
recipes.remove(<betterwithmods:axle_generator:1>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("WindMillA", "", 20, [<aspect:aqua>, <aspect:ordo>], <betterwithmods:axle_generator:1>, 
					[[<betterwithmods:material:10>, <betterwithmods:material:10>, <betterwithmods:material:10>], 
					 [<betterwithmods:material:10>, <ore:gearWood>, <betterwithmods:material:10>,], 
					 [<betterwithmods:material:10>, <betterwithmods:material:10>, <betterwithmods:material:10>]]);
					 
mods.compatskills.Requirement.addRequirement(<betterwithmods:wooden_gearbox>, "reskillable:agility|4", "reskillable:building|4");
mods.compatskills.Requirement.addRequirement(<betterwithmods:wooden_axle>, "reskillable:agility|3");

mods.compatskills.Requirement.addRequirement(<betterwithmods:steel_gearbox>, "reskillable:agility|7", "reskillable:building|7");
mods.compatskills.Requirement.addRequirement(<betterwithmods:steel_axle>, "reskillable:agility|4", "reskillable:building|5");

mods.compatskills.Requirement.addRequirement(<betterwithmods:axle_generator:2>, "reskillable:agility|3", "reskillable:building|4");
mods.compatskills.Requirement.addRequirement(<betterwithmods:axle_generator:1>, "reskillable:agility|2", "reskillable:building|4");
mods.compatskills.Requirement.addRequirement(<betterwithmods:axle_generator>, "reskillable:agility|2", "reskillable:building|4");

recipes.remove(<minecraft:gravel>);
recipes.addShaped(<minecraft:gravel>,
 [[<improvedextraction:small_rock>,<improvedextraction:small_rock>],
  [<improvedextraction:small_rock>,<improvedextraction:small_rock>]]);

recipes.remove(<primal_tech:fire_sticks>.withTag({}));
recipes.addShaped(<primal_tech:fire_sticks>.withTag({}),
 [[null,<ore:stickWood>],
  [<ore:stickWood>,null]]);

recipes.addShaped(<primal_tech:fibre_torch>,
 [[<tinkersurvival:grass_fiber>,null],
  [<ore:stickWood>,null]]);