import crafttweaker.item.IItemStack;



mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_decoration2:5>, "reskillable:defense|5", "reskillable:building|2");

//Sword
val BaseSword = [<betterwithmods:steel_sword:*>, <immersiveengineering:sword_steel:*>] as IItemStack[];

for i in BaseSword{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:attack|5");
}

mods.compatskills.Requirement.addRequirement(<astralsorcery:itemchargedcrystalsword:*>, "reskillable:attack|14", "reskillable:magic|5");
mods.compatskills.Requirement.addRequirement(<iceandfire:dragonbone_sword_fire:*>, "reskillable:attack|14", "reskillable:magic|5");
mods.compatskills.Requirement.addRequirement(<iceandfire:dragonbone_sword_ice:*>, "reskillable:attack|14", "reskillable:magic|5");

val AsGoldSword = [<iceandfire:silver_sword:*>,
					<thaumcraft:thaumium_sword:*>,
					<thaumcraft:void_sword:*>] as IItemStack[];

for i in AsGoldSword{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:attack|5", "reskillable:magic|7");
}

val AsDiamodSword = [<iceandfire:troll_weapon.axe:*>,
						<iceandfire:troll_weapon.column:*>, <iceandfire:troll_weapon.column_forest:*>,
						<iceandfire:troll_weapon.column_frost:*>, <iceandfire:troll_weapon.hammer:*>,
						<iceandfire:troll_weapon.trunk:*>, <iceandfire:troll_weapon.trunk_frost:*>,
						<iceandfire:dragonbone_sword:*>] as IItemStack[];

for i in AsDiamodSword{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:attack|16");
}

val GreateMagicSword = [<thaumcraft:primal_crusher:*>,
						<thaumcraft:crimson_blade:*>,<thaumcraft:elemental_sword:*>,
						<astralsorcery:itemcrystalsword:*>] as IItemStack[];

for i in GreateMagicSword{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:attack|16", "reskillable:magic|10");
}

//Axe and Shovel
val BaseAxe = [<immersiveengineering:shovel_steel:*>,<immersiveengineering:axe_steel:*>, <betterwithmods:steel_shovel:*>, <betterwithmods:steel_axe:*>, <betterwithmods:steel_battleaxe:*>] as IItemStack[];
					
val AsGoldAxe = [<iceandfire:silver_axe:*>,
						<thaumcraft:void_axe:*>, <thaumcraft:thaumium_axe:*>,
						<iceandfire:silver_shovel:*>,
						<thaumcraft:void_shovel:*>, <thaumcraft:thaumium_shovel:*>,] as IItemStack[];
						
val AsDiamodAxe = [<iceandfire:dragonbone_axe:*>, 
						<iceandfire:dragonbone_shovel:*>] as IItemStack[];
						
val GreateMagicAxe = [<thaumcraft:elemental_axe:*>, <astralsorcery:itemcrystalaxe:*>,
							<astralsorcery:itemchargedcrystalaxe:*>,
							<thaumcraft:elemental_axe:*>,
							<thaumcraft:elemental_shovel:*>, <astralsorcery:itemcrystalshovel:*>,
							<astralsorcery:itemchargedcrystalshovel:*>,
							<thaumcraft:elemental_shovel:*>] as IItemStack[];

for i in BaseAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:gathering|5");
}

for i in AsGoldAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:gathering|5", "reskillable:magic|5");
}

for i in AsDiamodAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:gathering|16");
}

for i in GreateMagicAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:gathering|14", "reskillable:magic|8");
}

//PickAxe

val BasePickAxe = [<betterwithmods:steel_pickaxe:*>, <immersiveengineering:pickaxe_steel:*>] as IItemStack[];

for i in BasePickAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:mining|5");
}

val AsDiamodPickAxe = [ <iceandfire:dragonbone_pickaxe:*>] as IItemStack[];

for i in AsDiamodPickAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:mining|16");
}

val AsGoldPickAxe = [<iceandfire:silver_pickaxe:*>,
						<thaumcraft:void_pick:*>, <thaumcraft:thaumium_pick:*>] as IItemStack[];

for i in AsGoldPickAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:mining|5", "reskillable:magic|5");
}

val GreateMagicPickAxe = [<thaumcraft:elemental_pick:*>, <astralsorcery:itemcrystalpickaxe:*>,
							<astralsorcery:itemchargedcrystalpickaxe:*>] as IItemStack[];

for i in GreateMagicPickAxe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:mining|14", "reskillable:magic|8");
}

//Hoe

val BaseHoe = [<betterwithmods:steel_hoe:*>, <betterwithmods:steel_mattock:*>] as IItemStack[];

for i in BaseHoe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:farming|5");
}

val AsDiamodHoe = [<iceandfire:dragonbone_hoe:*>] as IItemStack[];

for i in AsDiamodHoe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:farming|16");
}

val AsGoldHoe = [<iceandfire:silver_hoe:*>,
						<thaumcraft:thaumium_hoe:*>, <thaumcraft:void_hoe:*>] as IItemStack[];

for i in AsGoldHoe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:farming|5", "reskillable:magic|5");
}

val GreateMagicHoe = [<thaumcraft:elemental_hoe:*>] as IItemStack[];

for i in GreateMagicHoe{
	mods.compatskills.Requirement.addRequirement(i, "reskillable:farming|14", "reskillable:magic|8");
}

//pickaxe
mods.compatskills.Requirement.addRequirement(<tconstruct:pickaxe:*>
											.withTag({
											StatsOriginal:{HarvestLevel:2}}), "reskillable:mining|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:pickaxe:*>
											.withTag({
											Stats:{HarvestLevel:2}}), "reskillable:mining|5");

mods.compatskills.Requirement.addRequirement(<tconstruct:pickaxe:*>
											.withTag({
											StatsOriginal:{HarvestLevel:3}}), "reskillable:mining|10");
mods.compatskills.Requirement.addRequirement(<tconstruct:pickaxe:*>
											.withTag({
											Stats:{HarvestLevel:3}}), "reskillable:mining|10");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:pickaxe:*>
											.withTag({
											StatsOriginal:{HarvestLevel:4}}), "reskillable:mining|14");
mods.compatskills.Requirement.addRequirement(<tconstruct:pickaxe:*>
											.withTag({
											Stats:{HarvestLevel:4}}), "reskillable:mining|14");
											
//axe
mods.compatskills.Requirement.addRequirement(<tconstruct:lumberaxe:*>
											.withTag({
											StatsOriginal:{HarvestLevel:2}}), "reskillable:gathering|5", "reskillable:agility|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:lumberaxe:*>
											.withTag({
											Stats:{HarvestLevel:2}}), "reskillable:gathering|5", "reskillable:agility|3");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:lumberaxe:*>
											.withTag({
											StatsOriginal:{HarvestLevel:3}}), "reskillable:gathering|10", "reskillable:agility|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:lumberaxe:*>
											.withTag({
											Stats:{HarvestLevel:3}}), "reskillable:gathering|10", "reskillable:agility|6");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:lumberaxe:*>
											.withTag({
											StatsOriginal:{HarvestLevel:4}}), "reskillable:gathering|14", "reskillable:agility|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:lumberaxe:*>
											.withTag({
											Stats:{HarvestLevel:4}}), "reskillable:gathering|14", "reskillable:agility|8");
											
//1

mods.compatskills.Requirement.addRequirement(<tconstruct:hatchet:*>
											.withTag({
											StatsOriginal:{HarvestLevel:2}}), "reskillable:gathering|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:hatchet:*>
											.withTag({
											Stats:{HarvestLevel:2}}), "reskillable:gathering|5");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:hatchet:*>
											.withTag({
											StatsOriginal:{HarvestLevel:3}}), "reskillable:gathering|10");
mods.compatskills.Requirement.addRequirement(<tconstruct:hatchet:*>
											.withTag({
											Stats:{HarvestLevel:3}}), "reskillable:gathering|10");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:hatchet:*>
											.withTag({
											StatsOriginal:{HarvestLevel:4}}), "reskillable:gathering|14");
mods.compatskills.Requirement.addRequirement(<tconstruct:hatchet:*>
											.withTag({
											Stats:{HarvestLevel:4}}), "reskillable:gathering|14");
											
//2											
mods.compatskills.Requirement.addRequirement(<tconstruct:shovel:*>
											.withTag({
											StatsOriginal:{HarvestLevel:2}}), "reskillable:gathering|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:shovel:*>
											.withTag({
											Stats:{HarvestLevel:2}}), "reskillable:gathering|5");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:shovel:*>
											.withTag({
											StatsOriginal:{HarvestLevel:3}}), "reskillable:gathering|10");
mods.compatskills.Requirement.addRequirement(<tconstruct:shovel:*>
											.withTag({
											Stats:{HarvestLevel:3}}), "reskillable:gathering|10");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:shovel:*>
											.withTag({
											StatsOriginal:{HarvestLevel:4}}), "reskillable:gathering|14");
mods.compatskills.Requirement.addRequirement(<tconstruct:shovel:*>
											.withTag({
											Stats:{HarvestLevel:4}}), "reskillable:gathering|14");
//3

mods.compatskills.Requirement.addRequirement(<tconstruct:mattock:*>
											.withTag({
											StatsOriginal:{HarvestLevel:2}}), "reskillable:gathering|5", "reskillable:farming|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:mattock:*>
											.withTag({
											Stats:{HarvestLevel:2}}), "reskillable:gathering|5", "reskillable:farming|5");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:mattock:*>
											.withTag({
											StatsOriginal:{HarvestLevel:3}}), "reskillable:gathering|10", "reskillable:farming|7");
mods.compatskills.Requirement.addRequirement(<tconstruct:mattock:*>
											.withTag({
											Stats:{HarvestLevel:3}}), "reskillable:gathering|10", "reskillable:farming|7");
											
mods.compatskills.Requirement.addRequirement(<tconstruct:mattock:*>
											.withTag({
											StatsOriginal:{HarvestLevel:4}}), "reskillable:gathering|14", "reskillable:farming|10");
mods.compatskills.Requirement.addRequirement(<tconstruct:mattock:*>
											.withTag({
											Stats:{HarvestLevel:4}}), "reskillable:gathering|14", "reskillable:farming|10");
																						