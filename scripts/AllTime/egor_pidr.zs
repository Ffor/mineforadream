import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

recipes.remove(<minecraft:book>);
recipes.addShaped(<minecraft:book>,
 [[<ore:paper>,<ore:paper>, null],
  [<ore:paper>,<betterwithmods:material:32>, null],
  [null,null, null]]);

recipes.remove(<immersiveengineering:powerpack>);
recipes.addShaped(<immersiveengineering:powerpack>,
 [[null,null, null],
  [<betterwithmods:material:32>,<immersiveengineering:metal_device0>, <betterwithmods:material:32>],
  [<immersiveengineering:wirecoil>,<immersiveengineering:connector>, <immersiveengineering:wirecoil>]]);

recipes.remove(<tconstruct:piggybackpack>);
recipes.addShaped(<tconstruct:piggybackpack>,
 [[null,<ore:stickWood>, null],
  [<betterwithmods:material:32>,null, <betterwithmods:material:32>],
  [null,<ore:stickWood>, null]]);
	
recipes.remove(<immersiveengineering:toolupgrade:12>);
recipes.addShaped(<immersiveengineering:toolupgrade:12>,
 [[null,null, <betterwithmods:material:32>],
  [<betterwithmods:material:32>,<immersiveengineering:metal_decoration0>, null],
  [<betterwithmods:material:32>,<ore:ingotIron>, <betterwithmods:material:32>]]);

recipes.remove(<immersiveengineering:conveyor>.withTag({conveyorType: "immersiveengineering:conveyor"}));
recipes.addShaped(<immersiveengineering:conveyor>.withTag({conveyorType: "immersiveengineering:conveyor"})*8,
 [[null,null, null],
  [<betterwithmods:material:32>,<betterwithmods:material:32>, <betterwithmods:material:32>],
  [<ore:ingotIron>,<ore:dustRedstone>, <ore:ingotIron>]]);
  
  recipes.addShaped(<betterwithmods:leather_tanned_chest>,
 [[<betterwithmods:material:8>,null,<betterwithmods:material:8>],
  [<betterwithmods:material:32>,<betterwithmods:material:32>, <betterwithmods:material:32>],
  [<betterwithmods:material:32>,<betterwithmods:material:9>,<betterwithmods:material:32>]]);
  
  recipes.addShaped(<betterwithmods:leather_tanned_boots>,
 [[null,null,null],
  [<betterwithmods:material:32>,null, <betterwithmods:material:32>],
  [<betterwithmods:material:8>,null,<betterwithmods:material:8>]]);
  
  recipes.addShaped(<betterwithmods:leather_tanned_helmet>,
 [[<betterwithmods:material:32>,<betterwithmods:material:32>,<betterwithmods:material:32>],
  [<betterwithmods:material:8>,<betterwithmods:material:9>, <betterwithmods:material:8>],
  [null,null,null]]);

  recipes.addShaped(<betterwithmods:leather_tanned_pants>,
 [[<betterwithmods:material:32>,<betterwithmods:material:9>,<betterwithmods:material:32>],
  [<betterwithmods:material:32>,null, <betterwithmods:material:32>],
  [<betterwithmods:material:8>,null,<betterwithmods:material:8>]]);
 
 recipes.remove(<thaumcraft:baubles:2>);
recipes.addShaped(<thaumcraft:baubles:2>,
 [[null,<betterwithmods:material:9>, null],
  [null,<ore:ingotBrass>, null],
  [null,null, null]]);
  recipes.remove(<thaumcraft:baubles:6>);
recipes.addShaped(<thaumcraft:baubles:6>,
 [[null,<betterwithmods:material:9>, null],
  [null,<ore:gemDiamond>, null],
  [null,<ore:ingotGold>, null]]);
  
  recipes.remove(<conarm:travel_belt_base>);
recipes.addShaped(<conarm:travel_belt_base>,
 [[null,<ore:string>, null],
  [null,<betterwithmods:material:9>, null],
  [null,<ore:string>, null]]);
  
   recipes.remove(<conarm:travel_goggles_base>);
recipes.addShaped(<conarm:travel_goggles_base>,
 [[<betterwithmods:material:32>,<ore:string>, <betterwithmods:material:32>],
  [<ore:blockGlass>,<betterwithmods:material:32>, <ore:blockGlass>],
  [null,null, null]]);
  
  recipes.remove(<conarm:travel_cloak>);
recipes.addShaped(<conarm:travel_cloak>,
 [[<betterwithmods:material:32>,<ore:woolBlock>, <betterwithmods:material:32>],
  [<betterwithmods:material:32>,<ore:woolBlock>, <betterwithmods:material:32>],
  [<betterwithmods:material:32>,null, <betterwithmods:material:32>]]);
  
  recipes.remove(<conarm:travel_sack>);
recipes.addShaped(<conarm:travel_sack>,
 [[<ore:string>,<betterwithmods:material:32>, <ore:string>],
  [<betterwithmods:material:8>,<ore:chestWood>, <betterwithmods:material:8>],
  [<betterwithmods:material:32>,<betterwithmods:material:32>, <betterwithmods:material:32>]]);

  recipes.remove(<harvestcraft:hardenedleatheritem>);
recipes.addShaped(<harvestcraft:hardenedleatheritem>,
 [[<ore:materialPressedwax>,<betterwithmods:material:32>, null],
  [null,null, null],
  [null,null, null]]);
  
  recipes.remove(<conarm:gauntlet_mat>);
recipes.addShaped(<conarm:gauntlet_mat>,
 [[null,null, <ore:ingotIron>],
  [<ore:ingotIron>,<betterwithmods:material:32>, <ore:ingotIron>],
  [null,<ore:ingotIron>, <ore:ingotIron>]]);
  
   recipes.remove(<primal_tech:fluid_bladder>);
recipes.addShaped(<primal_tech:fluid_bladder>,
 [[<ore:string>,<betterwithmods:material:32>, null],
  [<betterwithmods:material:32>,null, <betterwithmods:material:32>],
  [null,<betterwithmods:material:32>, null]]);

recipes.addShaped(<toughasnails:canteen>,
 [[null,<betterwithmods:material:32>, null],
  [<betterwithmods:material:32>,null, <betterwithmods:material:32>],
  [<betterwithmods:material:32>,<betterwithmods:material:32>,<betterwithmods:material:32>]]);
  
  