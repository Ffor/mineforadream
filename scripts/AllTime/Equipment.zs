import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

recipes.remove(<tconstruct:throwball:1>);
recipes.addShaped(<tconstruct:throwball:1>,
 [[<minecraft:flint>,<ore:gunpowder>, <minecraft:flint>],
  [<ore:gunpowder>,<minecraft:coal:1>, <ore:gunpowder>],
  [<minecraft:flint>,<ore:gunpowder>, <minecraft:flint>]]);

	  