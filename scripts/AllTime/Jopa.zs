import crafttweaker.item.IItemStack;
//scythe
mods.compatskills.Requirement.addRequirement(<tconstruct:scythe:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:farming|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:scythe:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:farming|5");

mods.compatskills.Requirement.addRequirement(<tconstruct:scythe:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:farming|10");
mods.compatskills.Requirement.addRequirement(<tconstruct:scythe:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:farming|10");

mods.compatskills.Requirement.addRequirement(<tconstruct:scythe:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:farming|14");
mods.compatskills.Requirement.addRequirement(<tconstruct:scythe:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:farming|14");
//hammer
mods.compatskills.Requirement.addRequirement(<tconstruct:hammer:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:mining|5", "reskillable:attack|3", "reskillable:agility|2");
mods.compatskills.Requirement.addRequirement(<tconstruct:hammer:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:mining|5", "reskillable:attack|3", "reskillable:agility|2");

mods.compatskills.Requirement.addRequirement(<tconstruct:hammer:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:mining|10", "reskillable:attack|6", "reskillable:agility|4");
mods.compatskills.Requirement.addRequirement(<tconstruct:hammer:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:mining|10", "reskillable:attack|6", "reskillable:agility|4");
mods.compatskills.Requirement.addRequirement(<tconstruct:hammer:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:mining|14", "reskillable:attack|8", "reskillable:agility|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:hammer:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:mining|14", "reskillable:attack|8", "reskillable:agility|5");
//cleaver
mods.compatskills.Requirement.addRequirement(<tconstruct:cleaver:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|4");
mods.compatskills.Requirement.addRequirement(<tconstruct:cleaver:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|4");

mods.compatskills.Requirement.addRequirement(<tconstruct:cleaver:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|7");
mods.compatskills.Requirement.addRequirement(<tconstruct:cleaver:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|7");

mods.compatskills.Requirement.addRequirement(<tconstruct:cleaver:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|9");
mods.compatskills.Requirement.addRequirement(<tconstruct:cleaver:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|9");	
//battlesign			
mods.compatskills.Requirement.addRequirement(<tconstruct:battlesign:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:attack|3", "reskillable:agility|2");
mods.compatskills.Requirement.addRequirement(<tconstruct:battlesign:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:attack|3", "reskillable:agility|2");

mods.compatskills.Requirement.addRequirement(<tconstruct:battlesign:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:attack|7", "reskillable:agility|4");
mods.compatskills.Requirement.addRequirement(<tconstruct:battlesign:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:attack|7", "reskillable:agility|4");

mods.compatskills.Requirement.addRequirement(<tconstruct:battlesign:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:attack|11", "reskillable:agility|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:battlesign:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:attack|11", "reskillable:agility|6");	
//frypan
mods.compatskills.Requirement.addRequirement(<tconstruct:frypan:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:attack|3", "reskillable:farming|5", "reskillable:agility|2");
mods.compatskills.Requirement.addRequirement(<tconstruct:frypan:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:attack|3", "reskillable:farming|5", "reskillable:agility|2");

mods.compatskills.Requirement.addRequirement(<tconstruct:frypan:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:attack|6", "reskillable:farming|10", "reskillable:agility|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:frypan:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:attack|6", "reskillable:farming|10", "reskillable:agility|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:frypan:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:attack|8", "reskillable:farming|14", "reskillable:agility|4");
mods.compatskills.Requirement.addRequirement(<tconstruct:frypan:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:attack|8", "reskillable:farming|14", "reskillable:agility|4");
//shortbow	
mods.compatskills.Requirement.addRequirement(<tconstruct:shortbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:shortbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:shortbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:shortbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");

mods.compatskills.Requirement.addRequirement(<tconstruct:shortbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:shortbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");
//longbow
mods.compatskills.Requirement.addRequirement(<tconstruct:longbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:longbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:longbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:longbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");

mods.compatskills.Requirement.addRequirement(<tconstruct:longbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:longbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");	
//crossbow	
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");

mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");	
//shuriken
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:agility|5", "reskillable:attack|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:agility|10", "reskillable:attack|6");

mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:crossbow:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:agility|14", "reskillable:attack|8");	
//rapier
mods.compatskills.Requirement.addRequirement(<tconstruct:rapier:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|2");
mods.compatskills.Requirement.addRequirement(<tconstruct:rapier:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|2");

mods.compatskills.Requirement.addRequirement(<tconstruct:rapier:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|5");
mods.compatskills.Requirement.addRequirement(<tconstruct:rapier:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|5");

mods.compatskills.Requirement.addRequirement(<tconstruct:rapier:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|7");
mods.compatskills.Requirement.addRequirement(<tconstruct:rapier:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|7");
//broadsword
mods.compatskills.Requirement.addRequirement(<tconstruct:broadsword:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:broadsword:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:broadsword:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:broadsword:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|6");

mods.compatskills.Requirement.addRequirement(<tconstruct:broadsword:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:broadsword:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|8");
//longsword		
mods.compatskills.Requirement.addRequirement(<tconstruct:longsword	:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|3");
mods.compatskills.Requirement.addRequirement(<tconstruct:longsword	:*>
                                            .withTag({
                                            Stats:{HarvestLevel:2}}), "reskillable:attack|5", "reskillable:agility|3");

mods.compatskills.Requirement.addRequirement(<tconstruct:longsword	:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|6");
mods.compatskills.Requirement.addRequirement(<tconstruct:longsword	:*>
                                            .withTag({
                                            Stats:{HarvestLevel:3}}), "reskillable:attack|10", "reskillable:agility|6");

mods.compatskills.Requirement.addRequirement(<tconstruct:longsword	:*>
                                            .withTag({
                                            StatsOriginal:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|8");
mods.compatskills.Requirement.addRequirement(<tconstruct:longsword	:*>
                                            .withTag({
                                            Stats:{HarvestLevel:4}}), "reskillable:attack|14", "reskillable:agility|8");
										