import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;
import crafttweaker.oredict.IOreDictEntry;
import mods.primaltech.ClayKiln;
import mods.primaltech.StoneAnvil;

val OreRaw = [<geolosys:cluster>, <geolosys:cluster:1>, <geolosys:cluster:2>,
				<geolosys:cluster:3>, <geolosys:cluster:4>, <geolosys:cluster:5>,
				<geolosys:cluster:6>, <geolosys:cluster:7>, <geolosys:cluster:8>,
				<geolosys:cluster:9>, <geolosys:cluster:10>] as IItemStack[];
				
val IngotsODE = [<ore:ingotIron>, <ore:ingotGold>, <ore:ingotCopper>,
				<ore:ingotTin>, <ore:ingotSilver>, <ore:ingotLead>,
				<ore:ingotAluminum>, <ore:ingotNickel>, <ore:ingotPlatinum>,
				<ore:ingotUranium>, <ore:ingotZinc>] as IOreDictEntry[];
				
val IngotsODEtoAnvil = [<ore:ingotIron>, <ore:ingotGold>, <ore:ingotCopper>,
				<ore:ingotSilver>, <ore:ingotLead>,
				<ore:ingotAluminum>, <ore:ingotNickel>,
				<ore:ingotUranium>, <ore:ingotElectrum>,
				<ore:ingotConstantan>, <ore:ingotSteel>] as IOreDictEntry[];
				
val Plate = [<immersiveengineering:metal:39>, <immersiveengineering:metal:40>, <immersiveengineering:metal:30>,
			<immersiveengineering:metal:33>, <immersiveengineering:metal:32>, 
			<immersiveengineering:metal:31>, <immersiveengineering:metal:34>, 
			<immersiveengineering:metal:35>, <immersiveengineering:metal:37>,
			<immersiveengineering:metal:36>, <immersiveengineering:metal:38>] as IItemStack[];
			

		
for i in 0 to 11{
	StoneAnvil.addRecipe(Plate[i], IngotsODEtoAnvil[i]);
	mods.tconstruct.Casting.removeTableRecipe(Plate[i]);
}

mods.tconstruct.Casting.removeTableRecipe(<thaumcraft:plate>);
	
StoneAnvil.addRecipe(<immersiveengineering:metal:39>, <minecraft:iron_ingot>);	
	
val OreOre = [<ore:oreIron>, <ore:oreGold>, <ore:oreCopper>,
				<ore:oreTin>, <ore:oreSilver>, <ore:oreLead>,
				<ore:oreAluminum>, <ore:oreNickel>, <ore:orePlatinum>,
				<ore:oreUranium>, <ore:oreZinc>] as IOreDictEntry[];

val OreRawToL = [<geolosys:cluster:1>, <geolosys:cluster:2>,
				<geolosys:cluster:3>, <geolosys:cluster:4>, <geolosys:cluster:5>,
				<geolosys:cluster:6>,
				<geolosys:cluster:9>, <geolosys:cluster:10>] as IItemStack[];

val OreFragToL = [<improvedextraction:ore_fragment:1>, <improvedextraction:ore_fragment:2>,
					<improvedextraction:ore_fragment:3>, <improvedextraction:ore_fragment:4>, <improvedextraction:ore_fragment:5>,
					<improvedextraction:ore_fragment:6>,
					<improvedextraction:ore_fragment:15>, <improvedextraction:ore_fragment:11>] as IItemStack[];				
				
val L = [<liquid:gold>, <liquid:copper>,
			<liquid:tin>, <liquid:silver>, <liquid:lead>,
			<liquid:aluminum>,
			<liquid:uranium>, <liquid:zinc>] as ILiquidStack[];
			
for i in 0 to 8{
	mods.tconstruct.Melting.addRecipe(L[i]* 16,OreRawToL[i]);
	mods.tconstruct.Melting.addRecipe(L[i]* 32,OreFragToL[i]);
}				
for i in 0 to 11{
	OreOre[i].remove(OreRaw[i]);
}

<ore:oreIron>.remove(<geolosys:cluster>);

val Ingots = [<minecraft:iron_ingot>, <minecraft:gold_ingot>, <geolosys:ingot>,
				<geolosys:ingot:1>, <geolosys:ingot:2>, <geolosys:ingot:3>,
				<geolosys:ingot:4>, <geolosys:ingot:5>, <geolosys:ingot:6>,
				<immersiveengineering:metal:5>, <geolosys:ingot:7>] as IItemStack[];
				
for item in Ingots{
	recipes.remove(item);
	furnace.remove(item);
}



				
val Fragments = [<improvedextraction:ore_fragment>, <improvedextraction:ore_fragment:1>, <improvedextraction:ore_fragment:2>,
				<improvedextraction:ore_fragment:3>, <improvedextraction:ore_fragment:4>, <improvedextraction:ore_fragment:5>,
				<improvedextraction:ore_fragment:6>, <improvedextraction:ore_fragment:7>, <improvedextraction:ore_fragment:10>,
				<improvedextraction:ore_fragment:15>, <improvedextraction:ore_fragment:11>] as IItemStack[];
				
val FragmentsClaster = [<improvedextraction:ore_cluster>, <improvedextraction:ore_cluster:1>, <improvedextraction:ore_cluster:2>,
				<improvedextraction:ore_cluster:3>, <improvedextraction:ore_cluster:4>, <improvedextraction:ore_cluster:5>,
				<improvedextraction:ore_cluster:6>, <improvedextraction:ore_cluster:7>, <improvedextraction:ore_cluster:10>,
				<improvedextraction:ore_cluster:15>, <improvedextraction:ore_cluster:11>] as IItemStack[];
				

for i in 0 to 11{
	recipes.remove(FragmentsClaster[i]);
	mods.astralsorcery.Grindstone.addRecipe(OreRaw[i], Fragments[i], 0.55f);
	mods.horsepower.Grindstone.add(OreRaw[i], Fragments[i], 10, true, Fragments[i], 40);
	mods.immersiveengineering.Crusher.addRecipe(Fragments[i] * 4, OreRaw[i], 2048);
	mods.horsepower.Press.add(Fragments[i] * 9, FragmentsClaster[i]);
	mods.immersiveengineering.MetalPress.addRecipe(FragmentsClaster[i], Fragments[i], <immersiveengineering:mold:6>, 3000, 9);
}
