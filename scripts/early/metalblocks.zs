import mods.primaltech.WoodenBasin;
import mods.primaltech.ClayKiln;
import mods.tconstruct.Melting;
import crafttweaker.item.IItemStack;
import mods.primaltech.StoneAnvil;
import crafttweaker.liquid.ILiquidStack;


mods.tconstruct.Melting.addRecipe(<liquid:stone> * 144,<tconstruct:materials>);
mods.tconstruct.Melting.addRecipe(<liquid:stone> * 144,<tconstruct:soil>);


mods.compatskills.Requirement.addRequirement(<tcomplement:melter:*>, "reskillable:building|2");
mods.compatskills.Requirement.addRequirement(<primal_tech:stone_anvil>, "reskillable:building|4");


furnace.remove(<tconstruct:materials>, <tconstruct:soil>);


Melting.removeRecipe(<liquid:stone>);
recipes.remove(<thaumcraft:plate:*>);

recipes.remove(<primal_tech:stone_anvil>);
recipes.addShapeless(<primal_tech:stone_anvil>, [<primal_tech:stone_mallet:*>.transformDamage(10), <minecraft:iron_block>]);

StoneAnvil.addRecipe(<thaumcraft:plate:1>,<ore:ingotIron>);
StoneAnvil.addRecipe(<thaumcraft:plate>,<ore:ingotBrass>);
StoneAnvil.addRecipe(<thaumcraft:plate:2>,<thaumcraft:ingot>);
StoneAnvil.addRecipe(<thaumcraft:plate:3>,<ore:ingotVoid>);

mods.thaumcraft.Crucible.removeRecipe(<thaumcraft:ingot>);
mods.thaumcraft.Crucible.removeRecipe(<thaumcraft:ingot:2>);

mods.immersiveengineering.AlloySmelter.addRecipe(<thaumcraft:ingot>, <ore:ingotIron>, <thaumcraft:salis_mundus> * 4, 2000);

recipes.remove(<thaumcraft:stone_arcane>);
mods.tconstruct.Casting.addBasinRecipe(<thaumcraft:stone_arcane>, <thaumcraft:salis_mundus>, <liquid:stone>, 288, true, 200);

recipes.remove(<thaumcraft:stone_arcane_brick>);
mods.tconstruct.Casting.addBasinRecipe(<thaumcraft:stone_arcane_brick>, <thaumcraft:stone_arcane>, <liquid:brass>, 288, true, 200);


recipes.remove(<primal_tech:work_stump>);
recipes.addShaped(<primal_tech:work_stump>, [[<ore:ticSaw>], [<ore:logWood>]]);
recipes.addShaped(<primal_tech:work_stump>, [[<ore:crudeSaw>], [<ore:logWood>]]);
