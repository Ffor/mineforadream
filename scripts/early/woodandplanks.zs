import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

import mods.horsepower.ChoppingBlock;
//доски и палки

recipes.remove(<minecraft:planks:*>);
recipes.remove(<thaumcraft:plank_greatwood>);
recipes.remove(<thaumcraft:plank_silverwood>);

recipes.remove(<minecraft:stick>);

recipes.remove(<primal_tech:water_saw>);

ChoppingBlock.add(<minecraft:planks:*>, <minecraft:stick> * 4, 4, true);
ChoppingBlock.add(<minecraft:planks:*>, <minecraft:stick> * 4, 2, false);

ChoppingBlock.add(<thaumcraft:log_greatwood>, <thaumcraft:plank_greatwood> * 4, 4, true);
ChoppingBlock.add(<thaumcraft:log_greatwood>, <thaumcraft:plank_greatwood> * 4, 2, false);
ChoppingBlock.add(<thaumcraft:log_silverwood>, <thaumcraft:plank_silverwood> * 4, 4, true);
ChoppingBlock.add(<thaumcraft:log_silverwood>, <thaumcraft:plank_silverwood> * 4, 2, false);
ChoppingBlock.add(<thaumcraft:log_silverwood>, <thaumcraft:plank_silverwood> * 4, 2, false);


val WoodArray = [<biomesoplenty:log_0:4>, <biomesoplenty:log_0:5>, <biomesoplenty:log_0:6>,
				<biomesoplenty:log_0:7>, <biomesoplenty:log_1:4>, <biomesoplenty:log_1:5>,
				<biomesoplenty:log_1:6>, <biomesoplenty:log_1:7>, <biomesoplenty:log_2:4>,
				<biomesoplenty:log_2:5>, <biomesoplenty:log_2:6>, <biomesoplenty:log_2:7>,
				<biomesoplenty:log_3:4>, <biomesoplenty:log_3:5>, <biomesoplenty:log_3:6>,
				<biomesoplenty:log_3:7>] as IItemStack[];

val Planks = [<biomesoplenty:planks_0>, <biomesoplenty:planks_0:1>, <biomesoplenty:planks_0:2>,
			<biomesoplenty:planks_0:3>, <biomesoplenty:planks_0:4>,<biomesoplenty:planks_0:5>,
			<biomesoplenty:planks_0:6>, <biomesoplenty:planks_0:7>, <biomesoplenty:planks_0:8>,
			<biomesoplenty:planks_0:9>, <biomesoplenty:planks_0:10>, <biomesoplenty:planks_0:11>,
			<biomesoplenty:planks_0:12>, <biomesoplenty:planks_0:13>, <biomesoplenty:planks_0:14>,
			<biomesoplenty:planks_0:15>] as IItemStack[];
			
for item in Planks{
	recipes.remove(item);
}

for i in 0 to 16{
	ChoppingBlock.add(WoodArray[i], Planks[i] * 4, 1, true);
	ChoppingBlock.add(WoodArray[i], Planks[i] * 4, 1, false);
}

for i in 0 to 16{
	ChoppingBlock.add(Planks[i], <minecraft:stick> * 4, 4, true);
	ChoppingBlock.add(Planks[i], <minecraft:stick> * 4, 2, false);
} 

ChoppingBlock.add(<immersiveengineering:treated_wood:*>, <immersiveengineering:material> * 2, 4, true);
ChoppingBlock.add(<immersiveengineering:treated_wood:*>, <immersiveengineering:material> * 2, 2, false);  