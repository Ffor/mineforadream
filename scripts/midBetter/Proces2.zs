import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;
import crafttweaker.oredict.IOreDictEntry;
import mods.primaltech.ClayKiln;


mods.betterwithmods.Crucible.addStoked([<minecraft:soul_sand>, <ore:bone>, <minecraft:rotten_flesh>], [<betterwithmods:material:38>]);


val OreRaw = [<geolosys:cluster>, <geolosys:cluster:1>, <geolosys:cluster:2>,
				<geolosys:cluster:3>, <geolosys:cluster:4>, <geolosys:cluster:5>,
				<geolosys:cluster:6>, <geolosys:cluster:7>, <geolosys:cluster:8>,
				<geolosys:cluster:9>, <geolosys:cluster:10>] as IItemStack[];
				
val IngotsODE = [<ore:ingotIron>, <ore:ingotGold>, <ore:ingotCopper>,
				<ore:ingotTin>, <ore:ingotSilver>, <ore:ingotLead>,
				<ore:ingotAluminum>, <ore:ingotNickel>, <ore:ingotPlatinum>,
				<ore:ingotUranium>, <ore:ingotZinc>] as IOreDictEntry[];
				
val OreOre = [<ore:oreIron>, <ore:oreGold>, <ore:oreCopper>,
				<ore:oreTin>, <ore:oreSilver>, <ore:oreLead>,
				<ore:oreAluminum>, <ore:oreNickel>, <ore:orePlatinum>,
				<ore:oreUranium>, <ore:oreZinc>] as IOreDictEntry[];
				
val Ingots = [<minecraft:iron_ingot>, <minecraft:gold_ingot>, <geolosys:ingot>,
				<geolosys:ingot:1>, <geolosys:ingot:2>, <geolosys:ingot:3>,
				<geolosys:ingot:4>, <geolosys:ingot:5>, <geolosys:ingot:6>,
				<immersiveengineering:metal:5>, <geolosys:ingot:7>] as IItemStack[];

val Fragments = [<improvedextraction:ore_fragment>, <improvedextraction:ore_fragment:1>, <improvedextraction:ore_fragment:2>,
				<improvedextraction:ore_fragment:3>, <improvedextraction:ore_fragment:4>, <improvedextraction:ore_fragment:5>,
				<improvedextraction:ore_fragment:6>, <improvedextraction:ore_fragment:7>, <improvedextraction:ore_fragment:10>,
				<improvedextraction:ore_fragment:15>, <improvedextraction:ore_fragment:11>] as IItemStack[];
				
val FragmentsClaster = [<improvedextraction:ore_cluster>, <improvedextraction:ore_cluster:1>, <improvedextraction:ore_cluster:2>,
				<improvedextraction:ore_cluster:3>, <improvedextraction:ore_cluster:4>, <improvedextraction:ore_cluster:5>,
				<improvedextraction:ore_cluster:6>, <improvedextraction:ore_cluster:7>, <improvedextraction:ore_cluster:10>,
				<improvedextraction:ore_cluster:15>, <improvedextraction:ore_cluster:11>] as IItemStack[];
				
mods.compatskills.Requirement.addRequirement(<betterwithmods:single_machine>, "reskillable:building|3", "reskillable:gathering|2");

mods.compatskills.Requirement.addRequirement(<betterwithmods:hand_crank>, "reskillable:building|3");

for i in 0 to 10{
	mods.betterwithmods.Mill.addRecipe([OreRaw[i]],[Fragments[i]*2]);
}

mods.compatskills.Requirement.addRequirement(<immersiveengineering:stone_decoration>, "reskillable:building|5");
mods.compatskills.Requirement.addRequirement(<betterwithmods:single_machine:2>, "reskillable:building|5", "reskillable:gathering|3");
mods.compatskills.Requirement.addRequirement(<betterwithmods:hibachi>, "reskillable:building|6");

recipes.remove(<immersiveengineering:stone_decoration>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("Stone_dec", "", 30, [<aspect:ignis> * 3], <immersiveengineering:stone_decoration> * 3,
	[[<minecraft:clay_ball>, <ore:ingotBrick>, <minecraft:clay_ball>], 
	 [<ore:ingotBrick>, <ore:sandstone>, <ore:ingotBrick>], 
	 [<minecraft:clay_ball>, <ore:ingotBrick>, <minecraft:clay_ball>]]);
	 
mods.compatskills.Requirement.addRequirement(<immersiveengineering:tool>, "reskillable:building|6" , "reskillable:agility|3");
recipes.remove(<immersiveengineering:tool>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("ImmTool", "", 70, [<aspect:ordo> * 2, <aspect:terra> * 2], <immersiveengineering:tool>,
	[[null, <thaumcraft:ingot:2>, <ore:string>], 
	 [null, <ore:stickWood>, <thaumcraft:ingot:2>], 
	 [<ore:stickWood>, null, null]]);
  
  
mods.compatskills.Requirement.addRequirement(<immersiveengineering:stone_device>, "reskillable:building|6");

recipes.remove(<minecraft:glass>);
mods.compatskills.Requirement.addRequirement(<tconstruct:smeltery_controller:*>, "reskillable:building|9");
mods.compatskills.Requirement.addRequirement(<tconstruct:seared:3>, "reskillable:building|8");
recipes.remove(<tconstruct:smeltery_controller>);
mods.thaumcraft.ArcaneWorkbench.registerShapedRecipe("Smeltery", "", 1000, [<aspect:ordo> * 5, <aspect:terra> * 10, <aspect:ignis> * 10], <tconstruct:smeltery_controller>,
	[[<tconstruct:materials>, <tconstruct:materials>, <tconstruct:materials>], 
	 [<tconstruct:materials>, <iceandfire:fire_dragon_heart>, <tconstruct:materials>], 
	 [<tconstruct:materials>, <tconstruct:materials>, <tconstruct:materials>]]);
	 
mods.compatskills.Requirement.addRequirement(<immersiveengineering:stone_decoration:1>, "reskillable:building|7");
mods.compatskills.Requirement.addRequirement(<immersiveengineering:stone_device:1>, "reskillable:building|7", "reskillable:agility|5");

recipes.remove(<immersiveengineering:material:8>);
recipes.remove(<immersiveengineering:material:9>);

mods.compatskills.Requirement.addRequirement(<immersiveengineering:wooden_device0:2>, "reskillable:building|4", "reskillable:agility|4");
mods.compatskills.Requirement.addRequirement(<betterwithmods:steel_anvil:*>, "reskillable:building|5", "reskillable:agility|4", "reskillable:magic|5");

recipes.remove(<immersiveengineering:stone_decoration:1>);
recipes.addShaped(<immersiveengineering:stone_decoration:1>,
 [[<minecraft:netherbrick>,<minecraft:brick>, <minecraft:netherbrick>],
  [<minecraft:brick>,<betterwithmods:material:27>, <minecraft:brick>],
  [<minecraft:netherbrick>,<minecraft:brick>, <minecraft:netherbrick>]]);
  
  
recipes.remove(<immersiveengineering:metal_decoration0:3>);
mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_decoration0:3>, "reskillable:building|8", "reskillable:agility|5");
mods.thaumcraft.Infusion.registerRecipe("REB", "", <immersiveengineering:metal_decoration0:3>, 0, [<aspect:machina> * 10, <aspect:ordo> * 10], <immersiveengineering:metal_decoration1:5>, [<immersiveengineering:metal:31>, <betterwithmods:material:49>, <immersiveengineering:metal:31>, <immersiveengineering:wirecoil:5>, <immersiveengineering:metal:31>, <betterwithmods:material:34>, <immersiveengineering:metal:31>, <immersiveengineering:wirecoil>]);

recipes.remove(<immersiveengineering:metal_decoration0:4>);
mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_decoration0:4>, "reskillable:building|10", "reskillable:agility|5");
mods.thaumcraft.Infusion.registerRecipe("LEB", "", <immersiveengineering:metal_decoration0:4>, 0, [<aspect:machina> * 10, <aspect:ordo> * 10, <aspect:potentia> * 10], <immersiveengineering:metal_decoration1:5>, [<immersiveengineering:metal:31>, <betterwithmods:material:49>, <immersiveengineering:metal:31>, <immersiveengineering:material:8>, <immersiveengineering:metal:31>, <thaumcraft:mechanism_simple>, <immersiveengineering:metal:31>, <immersiveengineering:wirecoil>]);

recipes.remove(<immersiveengineering:metal_decoration0:5>);
mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_decoration0:5>, "reskillable:building|12", "reskillable:agility|6");
mods.thaumcraft.Infusion.registerRecipe("HEB", "", <immersiveengineering:metal_decoration0:5>, 0, [<aspect:machina> * 10, <aspect:ordo> * 10, <aspect:potentia> * 20], <immersiveengineering:metal_decoration1:1>, [<immersiveengineering:metal:38>, <betterwithmods:material:49>, <immersiveengineering:metal:38>, <immersiveengineering:material:9>, <immersiveengineering:metal:38>, <thaumcraft:mechanism_complex>, <immersiveengineering:metal:38>, <immersiveengineering:wirecoil:1>]);


recipes.remove(<immersiveengineering:material>);

recipes.remove(<immersiveengineering:wooden_device1>);
mods.thaumcraft.Crucible.registerRecipe("WWimm", "", <immersiveengineering:wooden_device1>, <betterwithmods:axle_generator:1>, [<aspect:machina> * 60, <aspect:aqua> * 30]);
mods.compatskills.Requirement.addRequirement(<immersiveengineering:wooden_device1>, "reskillable:building|6", "reskillable:agility|3");


recipes.remove(<immersiveengineering:wooden_device1:1>);
mods.thaumcraft.Crucible.registerRecipe("WMimm", "", <immersiveengineering:wooden_device1:1>, <betterwithmods:axle_generator>, [<aspect:machina> * 60, <aspect:aer> * 30]);
mods.compatskills.Requirement.addRequirement(<immersiveengineering:wooden_device1:1>, "reskillable:building|6", "reskillable:agility|3");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_decoration0:6>, "reskillable:building|7", "reskillable:agility|3");
mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_decoration0:7>, "reskillable:building|7", "reskillable:agility|3");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:connector:3>, "reskillable:building|3");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:connector:7>, "reskillable:building|5");
mods.compatskills.Requirement.addRequirement(<immersiveengineering:connector:8>, "reskillable:building|7");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_device1:2>, "reskillable:building|5");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_device1:3>, "reskillable:building|4");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_device1:7>, "reskillable:building|4", "reskillable:mining|5");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:metal_device1:5>, "reskillable:building|5", "reskillable:magic|5");

mods.compatskills.Requirement.addRequirement(<immersivepetroleum:metal_device:1>, "reskillable:building|5", "reskillable:agility|4");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:drill:*>, "reskillable:mining|5", "reskillable:agility|5", "reskillable:building|5");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:revolver>, "reskillable:attack|5", "reskillable:agility|5", "reskillable:building|5");

mods.compatskills.Requirement.addRequirement(<immersiveengineering:railgun>, "reskillable:attack|5", "reskillable:agility|5", "reskillable:building|7");


mods.compatskills.Requirement.addRequirement(<immersiveengineering:stone_decoration:10>, "reskillable:building|2", "reskillable:agility|2");
mods.compatskills.Requirement.addRequirement(<immersiveengineering:stone_device:7>, "reskillable:building|2", "reskillable:agility|2");