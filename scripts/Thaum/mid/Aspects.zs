import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

<iceandfire:dragonscales_red>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_green>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_gray>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_bronze>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_blue>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_white>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_sapphire>.setAspects(<aspect:draco>*12);
<iceandfire:dragonscales_silver>.setAspects(<aspect:draco>*12);

<iceandfire:fire_dragon_heart>.setAspects(<aspect:draco>*35);
<iceandfire:ice_dragon_heart>.setAspects(<aspect:draco>*35);