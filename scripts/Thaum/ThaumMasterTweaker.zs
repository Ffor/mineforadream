import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

import mods.primaltech.StoneAnvil;

recipes.remove(<thaumcraft:plate>);
recipes.remove(<thaumcraft:plate:2>);
recipes.remove(<thaumcraft:plate:3>);

mods.compatskills.Requirement.addRequirement(<thaumcraft:salis_mundus>, "reskillable:magic|2");
mods.compatskills.Requirement.addRequirement(<thaumcraft:scribing_tools>, "reskillable:magic|3", "reskillable:building|2");
mods.compatskills.Requirement.addRequirement(<thaumcraft:research_table:*>, "reskillable:magic|3");
mods.compatskills.Requirement.addRequirement(<thaumcraft:thaumonomicon>, "reskillable:magic|2");
mods.compatskills.Requirement.addRequirement(<thaumcraft:thaumometer>, "reskillable:magic|3", "reskillable:agility|2");


mods.compatskills.Requirement.addRequirement(<thaumcraft:caster_basic>, "reskillable:magic|4", "reskillable:agility|2");
mods.compatskills.Requirement.addRequirement(<thaumcraft:arcane_workbench_charger>, "reskillable:magic|6", "reskillable:building|2");

mods.compatskills.Requirement.addRequirement(<thaumcraft:lamp_arcane>, "reskillable:magic|3");

mods.compatskills.Requirement.addRequirement(<thaumcraft:lamp_growth>, "reskillable:magic|5", "reskillable:farming|5");
mods.compatskills.Requirement.addRequirement(<thaumcraft:lamp_fertility>, "reskillable:magic|5", "reskillable:farming|5");

mods.compatskills.Requirement.addRequirement(<thaumcraft:levitator>, "reskillable:magic|6", "reskillable:building|3");
mods.compatskills.Requirement.addRequirement(<thaumcraft:vis_battery>, "reskillable:magic|6", "reskillable:building|3", "reskillable:agility|3");


mods.compatskills.Requirement.addRequirement(<thaumcraft:centrifuge>, "reskillable:magic|8", "reskillable:building|3", "reskillable:agility|3", "reskillable:gathering|3");

mods.compatskills.Requirement.addRequirement(<thaumcraft:bellows>, "reskillable:magic|5", "reskillable:building|4");

mods.compatskills.Requirement.addRequirement(<thaumcraft:smelter_basic>, "reskillable:magic|7", "reskillable:gathering|4");

mods.compatskills.Requirement.addRequirement(<thaumcraft:smelter_thaumium>, "reskillable:magic|9", "reskillable:gathering|5");

mods.compatskills.Requirement.addRequirement(<thaumcraft:smelter_void>, "reskillable:magic|11", "reskillable:gathering|6");

mods.compatskills.Requirement.addRequirement(<thaumcraft:smelter_aux>, "reskillable:magic|7", "reskillable:gathering|5");
mods.compatskills.Requirement.addRequirement(<thaumcraft:smelter_vent>, "reskillable:magic|7", "reskillable:building|5");

mods.compatskills.Requirement.addRequirement(<thaumcraft:alembic>, "reskillable:magic|7", "reskillable:building|3", "reskillable:gathering|4");

mods.compatskills.Requirement.addRequirement(<thaumcraft:recharge_pedestal>, "reskillable:magic|7", "reskillable:building|2");

mods.compatskills.Requirement.addRequirement(<thaumcraft:wand_workbench>, "reskillable:magic|6", "reskillable:building|2");

mods.compatskills.Requirement.addRequirement(<thaumcraft:stone_arcane>, "reskillable:magic|3");
mods.compatskills.Requirement.addRequirement(<thaumcraft:stone_arcane_brick>, "reskillable:magic|3");

mods.compatskills.Requirement.addRequirement(<thaumcraft:tube>, "reskillable:magic|3", "reskillable:gathering|4");
mods.compatskills.Requirement.addRequirement(<thaumcraft:tube_valve>, "reskillable:magic|3", "reskillable:gathering|4");
mods.compatskills.Requirement.addRequirement(<thaumcraft:tube_restrict>, "reskillable:magic|3", "reskillable:gathering|4");
mods.compatskills.Requirement.addRequirement(<thaumcraft:tube_oneway>, "reskillable:magic|3", "reskillable:gathering|4");
mods.compatskills.Requirement.addRequirement(<thaumcraft:tube_filter>, "reskillable:magic|3", "reskillable:gathering|4");
mods.compatskills.Requirement.addRequirement(<thaumcraft:tube_buffer>, "reskillable:magic|4", "reskillable:gathering|4", "reskillable:building|2");

mods.compatskills.Requirement.addRequirement(<thaumcraft:infusion_matrix>, "reskillable:magic|13");
mods.compatskills.Requirement.addRequirement(<thaumcraft:brain_box>, "reskillable:magic|10");

mods.compatskills.Requirement.addRequirement(<thaumcraft:metal_alchemical>, "reskillable:magic|9", "reskillable:building|6");
mods.compatskills.Requirement.addRequirement(<thaumcraft:vis_generator>, "reskillable:magic|7", "reskillable:building|4");
mods.compatskills.Requirement.addRequirement(<thaumcraft:essentia_output>, "reskillable:magic|9", "reskillable:gathering|7");
mods.compatskills.Requirement.addRequirement(<thaumcraft:essentia_input>, "reskillable:magic|9", "reskillable:gathering|7");

mods.compatskills.Requirement.addRequirement(<thaumcraft:metal_alchemical>, "reskillable:magic|10", "reskillable:building|6");

mods.compatskills.Requirement.addRequirement(<thaumcraft:matrix_speed>, "reskillable:magic|10", "reskillable:building|6");
mods.compatskills.Requirement.addRequirement(<thaumcraft:matrix_cost>, "reskillable:magic|10", "reskillable:building|6");

mods.compatskills.Requirement.addRequirement(<thaumcraft:pattern_crafter>, "reskillable:magic|4", "reskillable:gathering|4");

mods.compatskills.Requirement.addRequirement(<thaumcraft:turret>, "reskillable:magic|7", "reskillable:building|4", "reskillable:defense|5");
mods.compatskills.Requirement.addRequirement(<thaumcraft:turret:1>, "reskillable:magic|7", "reskillable:building|4", "reskillable:defense|7");
mods.compatskills.Requirement.addRequirement(<thaumcraft:turret:2>, "reskillable:magic|7", "reskillable:building|4", "reskillable:mining|7");
mods.compatskills.Requirement.addRequirement(<thaumcraft:potion_sprayer>, "reskillable:magic|4", "reskillable:defense|5");

mods.compatskills.Requirement.addRequirement(<thaumcraft:thaumatorium:*>, "reskillable:magic|10", "reskillable:gathering|4", "reskillable:building|5");
mods.compatskills.Requirement.addRequirement(<thaumcraft:thaumatorium_top:*>, "reskillable:magic|7", "reskillable:gathering|4");