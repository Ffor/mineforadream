import crafttweaker.item.IIngredient;
import crafttweaker.item.IItemStack;
import crafttweaker.liquid.ILiquidStack;

import mods.thaumcraft.Infusion;

import mods.primaltech.ClayKiln;



recipes.remove(<tconstruct:metal:3>);
recipes.remove(<geolosys:ingot:1>);
recipes.remove(<immersiveengineering:metal>);
recipes.remove(<immersiveengineering:metal:1>);
recipes.remove(<tconstruct:ingots:3>);

mods.unidict.removalByKind.get("Crafting").remove("block", ["ingot", "nugget"]);
mods.unidict.removalByKind.get("Crafting").remove("ingot",["nugget"]);
mods.unidict.removalByKind.get("Crafting").remove("plate",["ingot"]);
mods.unidict.removalByKind.get("Furnace").remove("ingot",["dust"]);
mods.unidict.removalByKind.get("Crafting").remove("dust",["ore"]);

furnace.remove(<minecraft:netherbrick>, <minecraft:netherrack>);


//Dragon Armor
recipes.remove(<iceandfire:armor_red_helmet>);
recipes.remove(<iceandfire:armor_red_chestplate>); 
recipes.remove(<iceandfire:armor_red_leggings>); 
recipes.remove(<iceandfire:armor_red_boots>);

recipes.remove(<iceandfire:armor_blue_helmet>);
recipes.remove(<iceandfire:armor_blue_chestplate>);
recipes.remove(<iceandfire:armor_blue_leggings>);
recipes.remove(<iceandfire:armor_blue_boots>);

recipes.remove(<iceandfire:armor_bronze_helmet>);
recipes.remove(<iceandfire:armor_bronze_chestplate>);   
recipes.remove(<iceandfire:armor_bronze_leggings>);   
recipes.remove(<iceandfire:armor_bronze_boots>);

recipes.remove(<iceandfire:armor_gray_helmet>);
recipes.remove(<iceandfire:armor_gray_chestplate>);   
recipes.remove(<iceandfire:armor_gray_leggings>);   
recipes.remove(<iceandfire:armor_gray_boots>);

recipes.remove(<iceandfire:armor_green_helmet>);
recipes.remove(<iceandfire:armor_green_chestplate>);   
recipes.remove(<iceandfire:armor_green_leggings>);   
recipes.remove(<iceandfire:armor_green_boots>);

recipes.remove(<iceandfire:armor_sapphire_helmet>);
recipes.remove(<iceandfire:armor_sapphire_chestplate>);   
recipes.remove(<iceandfire:armor_sapphire_leggings>);   
recipes.remove(<iceandfire:armor_sapphire_boots>);

recipes.remove(<iceandfire:armor_silver_helmet>);
recipes.remove(<iceandfire:armor_silver_chestplate>);   
recipes.remove(<iceandfire:armor_silver_leggings>);   
recipes.remove(<iceandfire:armor_silver_boots>); 

recipes.remove(<iceandfire:armor_white_helmet>);
recipes.remove(<iceandfire:armor_white_chestplate>);   
recipes.remove(<iceandfire:armor_white_leggings>);   
recipes.remove(<iceandfire:armor_white_boots>);